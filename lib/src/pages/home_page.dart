import 'dart:io';

import 'package:flutter/material.dart';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:qrscanner/src/bloc/scans_bloc.dart';
import 'package:qrscanner/src/models/scan_model.dart';

import 'package:qrscanner/src/pages/direcciones_page.dart';
import 'package:qrscanner/src/pages/mapas_page.dart';
import 'package:qrscanner/src/utils/utils.dart' as utils;


class HomePage extends StatefulWidget {
 

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final scansBloc = new ScansBloc();
  int currentindex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Scanner'),
        actions: [
          IconButton(
           icon: Icon(Icons.delete_forever),
           onPressed:scansBloc.borrarScanTODOS,
           )
        ],
      ),
      body: _callPage(currentindex),
      bottomNavigationBar: _crearBottomNavigationBar(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.playlist_add_check),
        onPressed: () =>_scanQR(context),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _crearBottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: currentindex,
      onTap: (index) {
        setState(() {
          currentindex = index;
        });
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.pages), label: 'Mapas'),
        BottomNavigationBarItem(
            icon: Icon(Icons.directions), label: 'Direcciones')
      ],
    );
  }

  Widget _callPage(int paginaActual) {
    switch (paginaActual) {
      case 0:
        return MapasPage();
      case 1:
        return DireccionesPage();

      default:
        return MapasPage();
    }
  }

  _scanQR(BuildContext context) async {

    dynamic  futureString;
    //http://overskull.com/
    //geo:-12.115039,-77.0122072

    try {
     futureString = await BarcodeScanner.scan();
    } catch (e) {
       futureString.rawContent = e.toString();
    }

    print('FutureString : ${futureString.rawContent}');

    if (futureString != null) {
      final scan = ScanModel( valor: futureString.rawContent); 
      scansBloc.agregarScan(scan);

      if(Platform.isIOS){
        Future.delayed(Duration(milliseconds:  750),(){
          utils.abrirScan(context, scan);
        });
      }else{
        utils.abrirScan(context, scan);
      }
      
      

    }
  }
}
